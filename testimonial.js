docReady(() => {

    const arrows =  document.querySelectorAll('.testimonials__arrows .left, .testimonials__arrows .right');
    const track = document.querySelector('.testimonial-track');
    const SPEED = 10;
    arrows.forEach(a => {
        const dir = a.classList.value;
        let holding = null;

        a.addEventListener('mousedown', () => {
            holding = setInterval(scrollTrack, 1);
        });

        a.addEventListener('mouseup', () => {
            clearInterval(holding);
            holding = null;
        });

        function scrollTrack(){
            if(dir === 'left'){
                track.scroll(track.scrollLeft - SPEED, 0);
            }
            else if(dir === 'right'){
                track.scroll(track.scrollLeft + SPEED, 0);
            }
            else{
                console.error('Bizarre mec');
            }
        }
   
    });

});